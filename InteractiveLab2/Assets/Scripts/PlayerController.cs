using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Movement
    Vector3 mousePosition;
    float speed;
    public float baseSpeed;
    public float boostSpeed;
    Rigidbody2D rigidBody;

    // Velocity calculation
    Vector2 position;
    Vector2 newPosition;
    Vector2 oldPosition;
    Animator anim;
    float velocity;

    // Stats
    public int health;
    public GameObject heartPrefab;
    public Transform panelParent;
    List<GameObject> hearts = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        newPosition = transform.position; // Set first position for velocity calculation

        //Add health to max UI
        for (int i = 0; i < health; i++)
        {
            hearts.Add(Instantiate(heartPrefab, panelParent));
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Boost();
        Movement();
        CheckVelocity();
        Rotation();
        anim.SetFloat("Speed", speed);
        anim.SetFloat("Velocity", velocity);
    }

    // Player ship will move toward mouse current positon
    void Movement()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        rigidBody.transform.position = Vector2.Lerp(transform.position, mousePosition, speed);
        rigidBody.MovePosition(position);
    }

    // Player ship will rotate toward mouse current position
    void Rotation()
    {
        transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePosition - transform.position);
    }

    // Player ship will increase in speed while leftShift is held
    void Boost()
    {
        speed = Input.GetKey(KeyCode.LeftShift) ? boostSpeed : baseSpeed;
    }

    // Check the velocity, used to decide which animation should be playing
    void CheckVelocity()
    {
        oldPosition = newPosition;
        newPosition = transform.position;
        velocity = Mathf.Sqrt(Mathf.Pow((newPosition.x - oldPosition.x), 2)) + (Mathf.Pow((newPosition.y - oldPosition.y), 2));
    }

    // When player enters enemy decrease health by 1
    void OnTriggerEnter2D(Collider2D collision)
    {
       if(collision.gameObject.tag == "Enemy")
        {
            Destroy(hearts[health - 1].gameObject);
            health--;
            
            // If health reaches 0 move to GameOver scene
            if(health == 0)
            {
                GameManager.gameOver();
            }
        }
    }
}
