using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{
    // Change back to level scene when retry button pressed
    public void Retry() 
    {
        SceneManager.LoadScene("Level");
    }
}
