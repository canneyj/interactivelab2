using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameManager gameManager; // Reference to GamaManager
    Transform player; // Reference to player transform
    Rigidbody2D rigidBody; // Reference to rigidbody

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").transform; 
        rigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    // Calculate distance between enemy and player and move enemy
    void Update()
    {
        Vector2 playerDirection = player.position - transform.position;
        rigidBody.transform.Translate(playerDirection.x * Time.deltaTime, playerDirection.y * Time.deltaTime, 0);
    }

    // On collision with player destroy enemy
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
    