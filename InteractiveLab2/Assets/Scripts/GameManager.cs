using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Enemy enemyPrefab; // Refernce to the enemy prefab
    public float spawnInterval; //Interval of time between enemy spawns
    public int score = 0; // Current score
    public Text scoreText; // Score text for UI

    // Start is called before the first frame update
    // Spawn enemies constantly throughout runtime of the scene
    void Start()
    {
        InvokeRepeating("SpawnEnemy", 0, spawnInterval);
    }

    // Instantiate a new enemy at a randomly generated soot at the perimetre of the level
    void SpawnEnemy()
    {
        float randomSide = Random.Range(1, 4);
        float randomPoint = Random.Range(-10, 10);
        Vector2 spawnPosition;

        if(randomSide == 1) //Up
        {
            spawnPosition.x = randomPoint;
            spawnPosition.y = 10;
        }
        else if(randomSide == 2) //Down
        {
            spawnPosition.x = randomPoint;
            spawnPosition.y = -10;
        }
        else if(randomSide == 3) //Left
        {
            spawnPosition.x = -10;
            spawnPosition.y = randomPoint;
        }
        else //Right
        {
            spawnPosition.x = 10;
            spawnPosition.y = randomPoint;
        }

        Enemy enemyInstance = Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);
        enemyInstance.gameManager = this;

        score++; 
        scoreText.text = ("Score: " + score.ToString());
    }

    //Change scene to GameOver
    public static void gameOver()
    {
        SceneManager.LoadScene("GameOver");
    }
}
